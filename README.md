<div align="center"><img src="https://media.giphy.com/media/mGcNjsfWAjY5AEZNw6/giphy.gif" width="300"></div>

<div align="center"><h2>OMNI-SERVICE-VINCSS</h2></div>

 * This is the README file for the `OMNI-SERVICE-VINCSS` package.
 * @package OMNI-SERVICE-VINCSS
 * @version 1.0.2
 * @trannguyendanhuy2904  
 * @license [MIT]


## Installation

Require `ethan-vincss/vincss` using composer.

## Getting started

* This section provides information on how to get started with using the `OMNI-SERVICE-VINCSS` package.
* To get started with using the `OMNI-SERVICE-VINCSS` package, follow the recommended next steps outlined in this README.


## Usage

This package provides a `VincssServiceProvider` that you can register in your Laravel application. This service provider registers a `FidoController` that you can use for FIDO2 authentication.

Here's how you can use this package:

* Register the `VincssServiceProvider` class in the `boottrap/app` file.

```php
<?php

$app->register(\Ethan\Vincss\VincssServiceProvider::class);

$vincssServiceProvider = new VincssServiceProvider();

```

