<?php 
    $message = [
    "register-fido-success"                 => [
        "EN" => "[{0}] has register fido2 successful",
        "VI" => "[{0}] đã được đăng ký fido2 thành công",
    ],
    "something-went-wrong" => [
        "EN" => "Something went wrong. Please contact with Admin",
        "VI" => "Lỗi kết nối. Vui lòng liên hệ với quản trị hệ thống.",
    ],
    "missing-field" => [
        "EN" => "The {0} is required.",
        "VI" => "Vui lòng nhập {0}.",
    ],

    'username'  => [
        "EN" => "User Name",
        "VI" => "Tên đăng nhập",
    ],
];