<?php

/**
 * Routes for the authentication FIDO endpoints.
 */
$router->group(['prefix' => 'auth', 'middleware' => ['cors2', 'trimInput']], function ($router) {
	$router->group(['prefix' => 'sso'], function ($router) {
		$router->group(['prefix' => 'fido2'], function ($router) {
			/**
			 * Route for FIDO login.
			 */
			$router->get('/login', "\Ethan\Vincss\FidoController@login");

			/**
			 * Route for FIDO callback.
			 */
			$router->get('/callback', "\Ethan\Vincss\FidoController@callback");
			/**
			 * Route for pre-registering a FIDO2 device.
			 */
			$router->post('/pre-sign-up', "\Ethan\Vincss\FidoController@fido2_preregister");

			/**
			 * Route for registering a FIDO2 device.
			 */
			$router->post('/sign-up', "\Ethan\Vincss\FidoController@fido2_register");

			/**
			 * Route for pre-login with a FIDO2 device.
			 */
			$router->post('/pre-sign-in', "\Ethan\Vincss\FidoController@fido2_prelogin");

			/**
			 * Route for logging in with a FIDO2 device.
			 */
			$router->post('/sign-in', "\Ethan\Vincss\FidoController@fido2_login");
	});
});

});
